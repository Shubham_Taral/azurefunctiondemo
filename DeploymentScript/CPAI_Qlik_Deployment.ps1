# Need to install qlik cli to run the below code. Installation documentation is available at https://qlik.dev/libraries-and-tools/qlik-cli

# To initilase a new tenant. API keys have to enabled on the tenant and have to have the 'developer' role 

##############################################################
################# Configuration Section ######################
##############################################################

#SQL database details
$SQLConn = new-object PSObject
$SQLConn | Add-Member -MemberType NoteProperty -Name "SQLInstance" -Value "cpai-qa-sql-1.c4acb43340ab.database.windows.net"
$SQLConn | Add-Member -MemberType NoteProperty -Name "SQLWebConnInstance" -Value ""
$SQLConn | Add-Member -MemberType NoteProperty -Name "SQLUsername" -Value "ContractPodAi"
$SQLConn | Add-Member -MemberType NoteProperty -Name "SQLPassword" -Value "ContractPod2019!"
$SQLConn | Add-Member -MemberType NoteProperty -Name "SQLDatabase" -Value ""

# domain details
$srcTenantDomain = "us"
$destTenantDomain = "us"
$urlenvironment = "qa"
$CMSCentralDBName = "CMSCentral"

#Define filepath and log filename variable to export files
$curDir = Get-Location
$filePath = "$curDir\"
$logFile = ($filePath+"DeploymentLog.txt")

##############################################################
################# Function Definitions #######################
##############################################################

# Helper Functions
Function Write-Log {
    param(
        [Parameter(Mandatory = $true)][string] $message,
        [Parameter(Mandatory = $false)]
        [ValidateSet("INFO","WARN","ERROR")]
        [string] $level = "INFO"
    )
    # Create timestamp
    $timestamp = (Get-Date).toString("yyyy/MM/dd HH:mm:ss")
    # Append content to log file

    $logText = "$timestamp [$level] - $message"
    Add-Content -Path $logFile -Value $logText
    Write-Host $logText
}

#Function to check if space exists
function myspaceExists {
    param([String] $spaceName)
    $result = (qlik space ls | ConvertFrom-Json) | Where-Object name -eq $spaceName
    Write-Log -level INFO -message "spaceExists check: $spaceName :: $result // $result[0].id"
    if($result){
        return $result[0].id
    }
    return @()
}

function spaceExists{
    param([String] $spaceName, [String] $tenantname, [String] $apiKey)

    #Query a Space
    $hdrs = @{}
    $hdrs.Add("Authorization","Bearer "+$apiKey)
    $hdrs.Add("content-type","application/json")
    $Params = "name="+$spacename
    $url = $tenantname+"/api/v1/spaces?"+$Params
    $resp = Invoke-WebRequest -Uri $url -UseBasicParsing -Method Get -Headers $hdrs
    $spaceid = ($resp.Content | ConvertFrom-Json).data | Where-Object name -eq $spaceName | select id
    #($resp.Content | ConvertFrom-Json).data
    $sid = $spaceid.id

    if($sid){
        return $sid
    }
    return @()
}

#Function to check if app exists 
function appExists {
    param([String] $appName,[String] $spaceId)
    $result = (qlik item ls --resourceType app --spaceId $spaceId | ConvertFrom-Json) | Where-Object name -eq $appName
    if($result){
        return $result[0].resourceId
    }
    return @()
}

# Cleanup space
function cleanSpace{
        param([String] $spaceId)
        $apps = (qlik item ls --resourceType app --spaceId $spaceId | ConvertFrom-Json)
        foreach($app in $apps)
        {
            $appid = $app.resourceId
            qlik app rm $appid -q
            Write-Log -level INFO -message "Removed appId - $appid"
        }
}

#Function to export apps
function Export-App {
    param([string[]] $qlikApps,[string] $exportfilepath)
      foreach($appName in $qlikApps) {
                $spaceId = 'personal'    
                $appId = appExists -appName $appName -spaceId $spaceId
                if($appId.length -eq 0){
                    Write-Log -level INFO -message "Application $appName doesn't exist"
                }
                else {
                    Write-Log -level INFO -message "Exporting application $appName"

                    qlik app export $appId --output-file $exportfilepath$appName".qvf" -q
                    Write-Log -level INFO -message "Application $appName with ID $appId is exported succefully to $filePath"
                }
            }
            Clear-Variable spaceId
 }

function Create-QlikSpace {
    param([string[]] $spaceNames,[string] $spaceType,[string[]] $userList, [String] $tenantname, [String] $apiKey)
    foreach($spaceName in $spaceNames) {        
        $spaceId = spaceExists -spaceName $spaceName -tenantname $tenantname -apiKey $apiKey 
        if(!$spaceId) {
            Write-Log -level INFO -message "Creating space $spaceName"
            $spaceId = qlik space create --name $spaceName --type $spaceType -q
            Write-Log -level INFO -message "Space $spaceName of type $spaceType created successfully"
            Assign-UserNotExists -spaceId $spaceId -spaceType $spaceType -userList $userList
        } 
        else {
            Write-Log -level INFO -message "Space $spaceName-$spaceId exists"
        }
    }
}

function Assign-UserNotExists {
    param([string] $spaceId,[string] $spaceType,[string[]] $userList)
    foreach($userEmail in $userList) {
        Write-Log -level INFO -message "Adding $userEmail to space $spaceId-$spaceType"
        $spaceUserId = qlik user ls --email $userEmail -q
        qlik space assignment create --assigneeId $spaceUserId --spaceId $spaceId --roles "dataconsumer","facilitator" --type user -q
        Write-Log -level INFO -message "Successfully added $userEmail to space $spaceId-$spaceType"
    }
}

function Import-App {
    param([string[]] $spaceNames,[string[]] $qlikApps,[string] $exportfilepath, [String] $tenantname, [String] $apiKey)
    foreach($spaceName in $spaceNames) {
        $spaceId = spaceExists -spaceName $spaceName -tenantname $tenantname -apiKey $apiKey 
        if($spaceId){
            cleanSpace -spaceId $spaceId
            foreach($appName in $qlikApps) {
                $import_message = @()
                $appId = (appExists -appName $appName -spaceId $spaceId)
                
                if($appId.length -eq 0){
                    Write-Log -level INFO -message "Application $appName doesn't exist --> Importing $appName application into $spaceName-$spaceId for first time"
                    $import_message = $(qlik app import --file $exportfilepath$appName".qvf" --mode new --spaceId $spaceId --name $appName -q)    
                }
                else {
                    Write-Log -level INFO -message "Application $appName exist --> Replacing existing application by deleting and creating again"
                    Write-Log -level INFO -message "Removed Application $appName - $appId"
                    qlik app rm $appId -q
                    $import_message = $(qlik app import --file $exportfilepath$appName".qvf" --mode new --spaceId $spaceId --name $appName -q)             
                }
                
                if($import_message.length -eq 0){
                    Write-Log -level ERROR -message "Error in importing the application $appName`n"
                }
                else {
                    Write-Log -level INFO -message "Application $appName with id $import_message succefully imported into $spaceName`n"
                    
                    #Start-Sleep -Seconds 10
                    $forsheetAppId = (appExists -appName $appName -spaceId $spaceId)
                    Write-Log -level INFO -message "Get list of private sheets from $appName to make them public"
                    
                    #Get Sheet objects 
                    $sheets = ((qlik app object ls --app $forsheetAppId --json | ConvertFrom-Json) | Where-Object qType -eq "sheet")
                    if($sheets)
                    {
                        #Publish sheets from private to public
                        $sheets | foreach-object {
                            $sheetid = $_.qId
                            qlik app object publish $sheetid -a $forsheetAppId
                            Write-Log -level INFO -message "Sheet id - $sheetid published`n"
                        }
                        Write-Log -level INFO -message "All private sheets are published`n"
                    }
                    else
                    {
                        Write-Log -level INFO -message "No private sheets are available to publish`n"
                    }
                }
            }
        }
    }
}

function Publish-App {
    param([string[]] $sharedSpaceNames,[string[]] $managedSpaceNames,[string[]] $qlikApps, [String] $tenantname, [String] $apiKey)
    Write-Log -level INFO -message "Inside Publish App function for space: $sharedSpaceNames & $managedSpaceNames"
    
    $publishAppIds = foreach ($j in $sharedSpaceNames) {
                    $k = spaceExists -spaceName $j -tenantname $tenantname -apiKey $apiKey 
                    foreach ($i in $qlikApps) {appExists -appName $i -spaceId $k}
                    }
    Write-Log -level INFO -message "PublishAppIds: $publishAppIds"
    
    foreach($spaceName in $managedSpaceNames) {
        $spaceId_temp = spaceExists -spaceName $spaceName -tenantname $tenantname -apiKey $apiKey 
        if($spaceId_temp){
            Write-Log -level INFO -message "SharedSpaceId: $spaceId_temp"
            foreach($publishAppName in $qlikApps) {
                Write-Log -level INFO -message "Checking app: $publishAppName"
                $publishAppId = appExists -appName $publishAppName -spaceId $spaceId_temp
                if($publishAppId.length -eq 0){
                    foreach ($j in $sharedSpaceNames) {
                        $k = spaceExists -spaceName $j -tenantname $tenantname -apiKey $apiKey 
                        $sharedAppId = appExists -appName $publishAppName -spaceId $k
                        if($publishAppIds.Contains($sharedAppId))
                        {
                            qlik app publish create $sharedAppId --spaceId $spaceId_temp -q
                            Write-Log -level INFO -message "Application $publishAppName-$sharedAppId is published"
                        }
                    }
                }
                else {
                    Write-Log -level INFO -message "Application exists. Recreating application"
                    foreach ($j in $sharedSpaceNames) {
                        $k = spaceExists -spaceName $j -tenantname $tenantname -apiKey $apiKey 
                        $sharedAppId = appExists -appName $publishAppName -spaceId $k
                        if($publishAppIds.Contains($sharedAppId))
                        {
                            qlik app rm $publishAppId -q
                            Write-Log -level INFO -message "Removed application $publishAppId"
                            qlik app publish create $sharedAppId --spaceId $spaceId_temp -q
                            Write-Log -level INFO -message "Application $publishAppName-$sharedAppId is recreated"
                        }
                    }
                }
            }
        }
    }
}

function Get-ObjectId {
    param([string[]] $managedSpaceNames, [String] $tenantname, [String] $apiKey, [object[]] $QlikApps)
    $spaceObjectId = @()

    foreach($spaceName in $managedSpaceNames) {
        $managedSpaceId = (spaceExists -spaceName $spaceName -tenantname $tenantname -apiKey $apiKey)
        if($managedSpaceId) {
            foreach($publishApp in $QlikApps) {
                $appId = (qlik app ls --name $publishApp --spaceId $managedSpaceId -q)
                if($appId) {
                    $sheets = (qlik app object ls --app $appId --json | ConvertFrom-Json) | Where-Object qType -eq "sheet"
                    $sheets | foreach-object {
                        $spaceObjectIdLine = new-object PSObject
                        $spaceObjectIdLine | Add-Member -MemberType NoteProperty -Name "SpaceId" -Value $managedSpaceId
                        $spaceObjectIdLine | Add-Member -MemberType NoteProperty -Name "AppId" -Value $appId
                        $spaceObjectIdLine | Add-Member -MemberType NoteProperty -Name "SheetId" -Value $_.qId
                        $spaceObjectIdLine | Add-Member -MemberType NoteProperty -Name "DashboardName" -Value $_.title
                        $spaceObjectId += $spaceObjectIdLine
                    }
                }
            }
        }
    }
    return $spaceObjectId
}

function Import-SQLModule {
    $SQLModuleCheck = Get-Module -ListAvailable SqlServer
    if ($SQLModuleCheck -eq $null)
    {
        Write-Log -level INFO -message "SqlServer Module Not Found - Installing"

        # Not installed, trusting PS Gallery to remove prompt on install
        Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
        # Installing module, requires run as admin for -scope AllUsers, change to CurrentUser if not possible
        Install-Module -Name SqlServer -Scope AllUsers -Confirm:$false -AllowClobber
    }

    Import-Module SqlServer
}

function Insert-QlikObjId {
    param([object] $SQLConn, [object[]] $spaceObjectIdList)
    $SQLInstance = $SQLConn.SQLInstance
    $SQLDatabase = $SQLConn.SQLDatabase
    $SQLUsername = $SQLConn.SQLUsername
    $SQLPassword = $SQLConn.SQLPassword

    # Truncate table
    $SQLQuery = "USE $SQLDatabase
    
    TRUNCATE TABLE  [dbo].[AnalyticsDashboard];"
    # Running the Truncate query
    Invoke-Sqlcmd -query $SQLQuery -ServerInstance $SQLInstance -Username $SQLUsername -Password $SQLPassword

    $i=1
    # Inserting array into the new table we created
    $spaceObjectIdList | ForEach-Object { 
        $SpaceId = $_.SpaceId
        $AppId = $_.AppId
        $SheetId = $_.SheetId
        $DashboardName = $_.DashboardName

        if ($DashboardName -eq "Task Analytics")
        {
            # Creating the INSERT query using the variables defined
            $SQLQuery = "USE $SQLDatabase
            declare @rid int =0,
            @did int = (select Id from dbo.Dashboardsettingsgroups where DefaultLabel='Deep Sights')

            set @rid = (select Id from dbo.DashboardSettings where DefaultLabel = '$DashboardName')
            
            if(@rid>0)
            begin
                INSERT INTO [dbo].[AnalyticsDashboard] (SpaceId, AppId, ObjId, TypeOfObj, IsActive, DashboardName, Sequence,AdditionalParameters,AddedBy,AddedOn,DashboardHeight,DashboardSettingsId)
                VALUES('$SpaceId','$AppId','$SheetId','sheet',1,'$DashboardName',$i,'theme=contractpodai-dark&opt=ctxmenu,currsel&select=`$::ApplicationId,#ApplicationId#',1087,getdate(),800,@rid);
            end
            else
            begin
                INSERT INTO [dbo].[DashboardSettings](DashboardSettingsGroupId,Name,HasHelpText,DefaultLabel)
                VALUES(@did,lower(replace('$DashboardName',' ','')),1,'$DashboardName');

                set @rid = (select Id from dbo.DashboardSettings where DefaultLabel = '$DashboardName');

                INSERT INTO [dbo].[AnalyticsDashboard] (SpaceId, AppId, ObjId, TypeOfObj, IsActive, DashboardName, Sequence,AdditionalParameters,AddedBy,AddedOn,DashboardHeight,DashboardSettingsId)
                VALUES('$SpaceId','$AppId','$SheetId','sheet',1,'$DashboardName',$i,'theme=contractpodai-dark&opt=ctxmenu,currsel&select=`$::ApplicationId,#ApplicationId#',1087,getdate(),800,@rid);
            end"
    
            # Running the INSERT query
            Invoke-Sqlcmd -query $SQLQuery -ServerInstance $SQLInstance -Username $SQLUsername -Password $SQLPassword
            
        }
        else
        {
            $IsActive = 1
            if (($DashboardName -eq "Business Outcome") -or ($DashboardName -eq "Legal Intake Insights"))
            {
                $IsActive = 0
            }

            # Creating the INSERT query using the variables defined
            $SQLQuery = "USE $SQLDatabase
            declare @rid int =0,
            @did int = (select Id from dbo.Dashboardsettingsgroups where DefaultLabel='Deep Sights')

            set @rid = (select Id from dbo.DashboardSettings where DefaultLabel = '$DashboardName')

            if(@rid > 0)
            begin
                INSERT INTO [dbo].[AnalyticsDashboard] (SpaceId, AppId, ObjId, TypeOfObj, IsActive, DashboardName, Sequence,AdditionalParameters,AddedBy,AddedOn,DashboardHeight,DashboardSettingsId)
                VALUES('$SpaceId','$AppId','$SheetId','sheet',$IsActive,'$DashboardName',$i,'theme=contractpodai-dark&opt=ctxmenu,currsel&select=`$::ApplicationId,#ApplicationId#',1087,getdate(),700,@rid);
            end
            else
            begin
                INSERT INTO [dbo].[DashboardSettings](DashboardSettingsGroupId,Name,HasHelpText,DefaultLabel)
                VALUES(@did,lower(replace('$DashboardName',' ','')),1,'$DashboardName');

                set @rid = (select Id from dbo.DashboardSettings where DefaultLabel = '$DashboardName');

                INSERT INTO [dbo].[AnalyticsDashboard] (SpaceId, AppId, ObjId, TypeOfObj, IsActive, DashboardName, Sequence,AdditionalParameters,AddedBy,AddedOn,DashboardHeight,DashboardSettingsId)
                VALUES('$SpaceId','$AppId','$SheetId','sheet',$IsActive,'$DashboardName',$i,'theme=contractpodai-dark&opt=ctxmenu,currsel&select=`$::ApplicationId,#ApplicationId#',1087,getdate(),700,@rid);
            end"
    
            # Running the INSERT query
            Invoke-Sqlcmd -query $SQLQuery -ServerInstance $SQLInstance -Username $SQLUsername -Password $SQLPassword
            
        }
        $i+=1
    }

    # Run refresh on your SQL DB and you will see the new data inserted
    Write-Log -level INFO -message "Loaded Space, App and Sheet details to AnalyticsDashboard table"
}

function Update-FeatureMetadataJson {
    param([object] $SQLConn, [string] $subscriberId, [string[]] $managedSpaceNames, [string] $qlikTenant, [string] $apiKey, [string] $tenantDomain)
    $SQLInstance = $SQLConn.SQLInstance
    $SQLDatabase = $CMSCentralDBName
    $SQLUsername = $SQLConn.SQLUsername
    $SQLPassword = $SQLConn.SQLPassword

    #Variables - details of the connection, stored procedure and parameters
    $connectionString = "server="+$SQLInstance+";database="+$SQLDatabase+";user id="+$SQLUsername+";password="+$SQLPassword+";";
    $queryText = "select FeatureMetadataJSON from SubscriberFeatures
where subscriberid=$subscriberId and FeatureId in (select FeatureId from Features where FeatureName='AnalyticsDashboard')
";
 
    #SQL Connection - connection to SQL server
    $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
    $sqlConnection.ConnectionString = $connectionString;
 
    #SQL Command - set up the SQL call
    $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
    $sqlCommand.Connection = $sqlConnection;
    $sqlCommand.CommandText = $queryText;
 
    #SQL Adapter - get the results using the SQL Command
    $sqlAdapter = new-object System.Data.SqlClient.SqlDataAdapter
    $sqlAdapter.SelectCommand = $sqlCommand
    $dataSet = new-object System.Data.Dataset
    $recordCount = $sqlAdapter.Fill($dataSet)
 
    #Close SQL Connection
    $sqlConnection.Close();

    foreach($spaceName in $managedSpaceNames) {
        $tenantname = "https://cpai-$qlikTenant.$tenantDomain.qlikcloud.com"
        
        #Query a Space
        $hdrs = @{}
        $hdrs.Add("Authorization","Bearer "+$apiKey)
        $hdrs.Add("content-type","application/json")
        $Params = "name="+$spacename
        $url = $tenantname+"/api/v1/spaces?"+$Params
        $resp = Invoke-WebRequest -Uri $url -UseBasicParsing -Method Get -Headers $hdrs
        $spaceid = ($resp.Content | ConvertFrom-Json).data | Where-Object name -eq $spaceName | select id
        #($resp.Content | ConvertFrom-Json).data
        $sid = $spaceid.id

        $appId = appExists -appName "1.0 Master ETL" -spaceId $sid
                
        #Get single table from dataset
        $data = $dataSet.Tables[0]
        $featureMetadataJSON = $data.FeatureMetadataJSON
        $compliedString = ' "MasterEtlAppId":"'+$appId+'", "SpaceId": "'+$sid+'" }'

        # Creating the update query using the variables defined
        $sqlQuery = "USE $SQLDatabase
        if(charindex('MasterEtlAppId','$featureMetadataJSON')=0)
        begin
            update SubscriberFeatures set FeatureMetadataJSON = LEFT('$featureMetadataJSON', (LEN('$featureMetadataJSON') - CHARINDEX(REVERSE('SpaceId'), REVERSE('$featureMetadataJSON')) + 1) - LEN('SpaceId') - LEN('], ') )+ '$compliedString'
            where subscriberid=$subscriberId and FeatureId in (select FeatureId from Features where FeatureName='AnalyticsDashboard')
        end
        else
        begin
            update SubscriberFeatures set FeatureMetadataJSON = LEFT('$featureMetadataJSON', (LEN('$featureMetadataJSON') - CHARINDEX(REVERSE('MasterEtlAppId'), REVERSE('$featureMetadataJSON')) + 1) - LEN('MasterEtlAppId') - LEN('], ') )+ '$compliedString'
            where subscriberid=$subscriberId and FeatureId in (select FeatureId from Features where FeatureName='AnalyticsDashboard')
        end
        "
        
        # Running the INSERT query
        Invoke-Sqlcmd -query $sqlQuery -ServerInstance $SQLInstance -Username $SQLUsername -Password $SQLPassword
        

        # Updating Url domain
        $SQLDatabase = $SQLConn.SQLDatabase
        $sqlQuery = "USE $SQLDatabase
		
		if not exists (select 1 from mst.UrlDomain)
		begin
			insert into mst.UrlDomain(Url) (select ('https://$urlenvironment.contractpod.com/'+url+'/vue-app/index.aspx/#/contract-snapshot/''&RequestId&''/redirect') from $CMSCentralDBName.dbo.Subscribers where DBName = (select DB_NAME()))
		end
		else
		begin
			update mst.UrlDomain set Url=(select ('https://$urlenvironment.contractpod.com/'+url+'/vue-app/index.aspx/#/contract-snapshot/''&RequestId&''/redirect') from $CMSCentralDBName.dbo.Subscribers where DBName = (select DB_NAME()))
		end
        "
        
        # Running the INSERT query
        Invoke-Sqlcmd -query $sqlQuery -ServerInstance $SQLInstance -Username $SQLUsername -Password $SQLPassword

    }
}

function Create-DataConnection {
    param([object] $SQLConn, [string[]] $managedSpaceNames, [string] $qlikTenant, [string] $apiKey, [string] $tenantDomain)

    $SQLWebConnInstance = $SQLConn.SQLWebConnInstance
    $SQLDatabase = $SQLConn.SQLDatabase
    $SQLUsername = $SQLConn.SQLUsername
    $SQLPassword = $SQLConn.SQLPassword
    
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

    foreach($spaceName in $managedSpaceNames) {
        $tenantname = "https://cpai-$qlikTenant.$tenantDomain.qlikcloud.com"
        
        #Query a Space
        $hdrs = @{}
        $hdrs.Add("Authorization","Bearer "+$apiKey)
        $hdrs.Add("content-type","application/json")
        $Params = "name="+$spacename
        $url = $tenantname+"/api/v1/spaces?"+$Params
        $resp = Invoke-WebRequest -Uri $url -UseBasicParsing -Method Get -Headers $hdrs
        $spaceid = ($resp.Content | ConvertFrom-Json).data | Where-Object name -eq $spaceName | select id
        #($resp.Content | ConvertFrom-Json).data
        $sid = $spaceid.id

        #Query connection
        $hdrs = @{}
        $hdrs.Add("Authorization","Bearer "+$apiKey)
        $hdrs.Add("content-type","application/json")
        $url = $tenantname+"/api/v1/data-connections?spaceId=$sid"
        $resp = Invoke-WebRequest -Uri $url -UseBasicParsing -Method Get -Headers $hdrs
        $data = ($resp.Content | ConvertFrom-Json).data | Where-Object {$_.qName -eq "CPAI_AZURE_SQL_TENANT"} | select qName

        if(!$data)
        {
            #Create connection
            $hdrs = @{}
            $hdrs.Add("Authorization","Bearer "+$apiKey)
            $hdrs.Add("content-type","application/json")
            $url = $tenantname+"/api/v1/data-connections"
            $bodycontent = '
            {
            "qName": "CPAI_AZURE_SQL_TENANT",
            "qConnectStatement": "provider=QvOdbcConnectorPackage.exe;driver=azure_sql;host='+$SQLWebConnInstance+';port=3342;database='+$SQLDatabase+';separateCredentials=true;allowNonSelectQueries=false;useBulkReader=false;bulkFetchSize=50;rowBatchSize=1;bulkFetchColumnMode=true;maxStringLength=4096;maxTotalBufferSize=4;maxFieldBufferSize=16;QueryTimeout=30;EnableQuotedIdentifiers=1;ReturnSpecificTypeAsOdbcType=yes;Min_TLS=1.0;",
            "qType":"QvOdbcConnectorPackage.exe",
            "qUsername": "'+$SQLUsername+'",
            "qPassword": "'+$SQLPassword+'",
            "qSeparateCredentials": true,
            "space": "'+$sid+'",
            "datasourceID": "azure_sql"
            }
            '
            $resp = Invoke-WebRequest -Uri $url -UseBasicParsing -Method Post -Headers $hdrs -Body $bodycontent
        }
    }
}

function Reload-Data{
    param([string] $managedSpaceNames, [String] $tenantname, [String] $apiKey)
    $status = ""
    foreach($spaceName in $managedSpaceNames) {
        $spaceId = spaceExists -spaceName $spaceName -tenantname $tenantname -apiKey $apiKey 
        foreach($appName in $qlikApps) {
            $appId = appExists -appName $appName -spaceId $spaceId
            $reloadId = (qlik reload create --appId $appId --json | ConvertFrom-Json) | select id
            Start-Sleep -s 30
            $reloadStatus = (qlik reload get $reloadId.id --json | ConvertFrom-Json) | select  status
            
            $status +=  "AppId: $appId - $reloadStatus`n"
        }
    }
    return $status
}

function Reload-Task{
    param([string[]] $managedSpaceNames,[string[]] $qlikApps, [String] $tenantname, [String] $apiKey)
    foreach($spaceName in $managedSpaceNames) {
        $spaceId = spaceExists -spaceName $spaceName -tenantname $tenantname -apiKey $apiKey 
        foreach($appName in $qlikApps) {
            $appId = appExists -appName $appName -spaceId $spaceId
            
            $reloadTaskId = (qlik reload-task ls --appId $appId --json | ConvertFrom-Json) | select id
            
            if($reloadTaskId)
            {
                $rid = $reloadTaskId.id
                Write-Host "Appname: $appName - ReloadTaskid - $rid - delete started"
                qlik reload-task rm $rid
                Write-Host "Appname: $appName - ReloadTaskid - $rid - delete compelted"
            }
            else
            {
                if($appName -eq "1.0 Master ETL")
                {
                    $startDate = ([DateTime]::UtcNow.ToString('yyyy-MM-ddTHH:mm:ss'))
                    qlik reload-task create --appId $appId --startDateTime $startDate --timeZone "Asia/Calcutta" --recurrence  "RRULE:FREQ=HOURLY;INTERVAL=1;BYMINUTE=01;BYSECOND=0" --type "external"
                }
                else
                {
                    $startDate = ([DateTime]::UtcNow.ToString('yyyy-MM-ddTHH:mm:ss'))
                    qlik reload-task create --appId $appId --startDateTime $startDate --timeZone "Asia/Calcutta" --recurrence  "RRULE:FREQ=HOURLY;INTERVAL=1;BYMINUTE=10;BYSECOND=0" --type "external"
                }
            }
        }
    }
}

function Get-ConfigurationFromSQL {
    param([object] $SQLConn)
    $SQLInstance = $SQLConn.SQLInstance
    $SQLDatabase = $CMSCentralDBName
    $SQLUsername = $SQLConn.SQLUsername
    $SQLPassword = $SQLConn.SQLPassword

    #Variables - details of the connection, stored procedure and parameters
    $connectionString = "server="+$SQLInstance+";database="+$SQLDatabase+";user id="+$SQLUsername+";password="+$SQLPassword+";";
    $queryText = "select s.[SubscriberId],s.[url] as TenantUrl,
replace(s.[url],reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))),'')+
case when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =1 then '_one'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =2 then '_two'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =3 then '_three'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =4 then '_four'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =5 then '_five'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =6 then '_six'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =7 then '_seven'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =8 then '_eight'
	when reverse(LEFT(reverse(s.[url]), PATINDEX('%[0-9][^0-9]%', reverse(s.[url]) ))) =9 then '_nine'
	else ''
end spacename
,s.DBName,sf.SubscriberId,sf.FeatureDefinitionJSON
from dbo.Features f 
join dbo.SubscriberFeatures sf 
	on f.featureId=sf.featureId 
    and f.featurename='AnalyticsDashboard' 
    and sf.FeatureMetadataJSON is not null 
    and sf.FeatureDefinitionJSON is not null
join dbo.Subscribers s on s.SubscriberId = sf.SubscriberId 
    and s.isActive='Y'
order by DBName
";
 
    #SQL Connection - connection to SQL server
    $sqlConnection = new-object System.Data.SqlClient.SqlConnection;
    $sqlConnection.ConnectionString = $connectionString;
 
    #SQL Command - set up the SQL call
    $sqlCommand = New-Object System.Data.SqlClient.SqlCommand;
    $sqlCommand.Connection = $sqlConnection;
    $sqlCommand.CommandText = $queryText;
 
    #SQL Adapter - get the results using the SQL Command
    $sqlAdapter = new-object System.Data.SqlClient.SqlDataAdapter
    $sqlAdapter.SelectCommand = $sqlCommand
    $dataSet = new-object System.Data.Dataset
    $recordCount = $sqlAdapter.Fill($dataSet)
 
    #Close SQL Connection
    $sqlConnection.Close();
 
    #Get single table from dataset
    $data = $dataSet.Tables[0]
 
    return $data
}

##############################################################
################# Main Implementation ########################
##############################################################

$jsonConfig = (Get-ConfigurationFromSQL -SQLConn $SQLConn)
$downloadQlikApp = 0

<#
foreach($config in $jsonConfig) {
    $SQLConn.SQLDatabase = $config.DBName
    $jsonData = ($config.FeatureDefinitionJSON | ConvertFrom-Json)
    $destTenant= $jsonData.DestinationQlikInstance
    $destApiKey = $jsonData.DestinationQlikInstanceAPIKey

    #Define an array with names of apps to be exported
    $QlikApps = New-Object System.Collections.ArrayList
    
    $jsonQlikApps = $jsonData.QlikApps
    foreach($jsonQlikApp in $jsonQlikApps) {
        $QlikApps.Add($jsonQlikApp)
    }
    $QlikApps

    $managedName = "managed_"+$config.spacename
    $ManagedSpaceName = New-Object System.Collections.ArrayList
    $ManagedSpaceName.Add($managedName)
    $ManagedSpaceName
    
    $qlikContext=("cpai-$destTenant.$destTenantDomain.qlikcloud.com")
    #connect to Qlik tenant
    qlik context use $qlikContext

    $tenantUrl = ("https://cpai-$destTenant.$destTenantDomain.qlikcloud.com")
    #Getting new space, app and sheet ids to load the SQL configuration table
    $spaceObjectIdList = Get-ObjectId -managedSpaceNames $ManagedSpaceName -tenantname $tenantUrl -apiKey $destApiKey -QlikApps $QlikApps
    $spaceObjectIdList    
    #Insert-QlikObjId -SQLConn $SQLConn -spaceObjectIdList $spaceObjectIdList
    
    #Update-FeatureMetadataJson -SQLConn $SQLConn -subscriberId $config.SubscriberId -managedSpaceNames $ManagedSpaceName -qlikTenant $destTenant -apiKey $destApiKey -tenantDomain $destTenantDomain

    #Reload-Task -managedSpaceNames $ManagedSpaceName -qlikApps $QlikApps -tenantname $tenantUrl -apiKey $destApiKey 
}
#>


foreach($config in $jsonConfig) {

    $dbname = $config.DBName
    $downloadQlikApp += 1

    Write-Log -level INFO -message "################# Deployment Started for: $dbname #############`n"

    $jsonData = ($config.FeatureDefinitionJSON | ConvertFrom-Json)
        
    # Define Qlik instances
    $srcTenant= "dev"
    $destTenant= $jsonData.DestinationQlikInstance
    $destApiKey = $jsonData.DestinationQlikInstanceAPIKey

    #Define an array with names of apps to be exported
    $QlikApps = New-Object System.Collections.ArrayList
    
    $jsonQlikApps = $jsonData.QlikApps
    foreach($jsonQlikApp in $jsonQlikApps) {
        $QlikApps.Add($jsonQlikApp)
    }
    
    #Define Space names
    $sharedName = "shared_"+$config.spacename
    $SharedSpaceName = New-Object System.Collections.ArrayList
    $SharedSpaceName.Add($sharedName)

    $managedName = "managed_"+$config.spacename
    $ManagedSpaceName = New-Object System.Collections.ArrayList
    $ManagedSpaceName.Add($managedName)

    $spacesUserList = New-Object System.Collections.ArrayList
    $jsonUsers = $jsonData.UserList
    foreach($jsonUser in $jsonUsers) {
        $spacesUserList.Add($jsonUser)
    }
    
    #Update database name
    $SQLConn.SQLDatabase = $dbname

    ##############################################
    ###########Calling Functions##################
    ##############################################
    Write-Log -level INFO -message "Started Deployment"

    #Lists all the tenants that are available to cli
    qlik context ls

    $qlikContext= ("cpai-$srcTenant.$srcTenantDomain.qlikcloud.com")
    #connect to qlik instance
    qlik context use $qlikContext

    Write-Log -level INFO -message "Shared space names: $SharedSpaceName`n"
    Write-Log -level INFO -message "Managed space names: $ManagedSpaceName`n"
    Write-Log -level INFO -message "User list: $spacesUserList`n"

    if($downloadQlikApp -eq 1)
    {
        #gets shared spaceid and exports the apps in that shared space into local drive 
        Write-Log -level INFO -message "-------------Exporting apps to local folder----------------"
        Export-App -qlikApps $QlikApps -exportfilepath $filePath
    }

    #Switch to cpai-qa tenant
    #qlik context use cpai-poc.us.qlikcloud.com

    $qlikContext=("cpai-$destTenant.$destTenantDomain.qlikcloud.com")
    #connect to Qlik tenant
    qlik context use $qlikContext

    $tenantUrl = ("https://cpai-$destTenant.$destTenantDomain.qlikcloud.com")
    #Creates space if doesn't exist 
    Write-Log -level INFO -message "-------------Creating Spaces and assigning users if doesn't exist----------------"
    Create-QlikSpace -spaceNames $SharedSpaceName -spaceType "shared" -userList $spacesUserList -tenantname $tenantUrl -apiKey $destApiKey 
    Create-QlikSpace -spaceNames $ManagedSpaceName -spaceType "managed" -userList $spacesUserList -tenantname $tenantUrl -apiKey $destApiKey 
    

    Write-Log -level INFO -message "-------------Importing apps to shared space and making sheets public----------------"
    Import-App -spaceNames $SharedSpaceName -qlikApps $QlikApps -exportfilepath $filePath -tenantname $tenantUrl -apiKey $destApiKey 
    
    
    Write-Log -level INFO -message "-------------Publising apps to a managed space and if exists it will be replaced----------------"
    Write-Log -level INFO -message "Apps to be published:`n$QlikApps"
    Publish-App -sharedSpaceNames $SharedSpaceName -managedSpaceNames $ManagedSpaceName -qlikApps $QlikApps -tenantname $tenantUrl -apiKey $destApiKey 
    
    #Getting new space, app and sheet ids to load the SQL configuration table
    $spaceObjectIdList = Get-ObjectId -managedSpaceNames $ManagedSpaceName -tenantname $tenantUrl -apiKey $destApiKey -QlikApps $QlikApps
    
    Import-SQLModule
    
    Insert-QlikObjId -SQLConn $SQLConn -spaceObjectIdList $spaceObjectIdList
    
    Update-FeatureMetadataJson -SQLConn $SQLConn -subscriberId $config.SubscriberId -managedSpaceNames $ManagedSpaceName -qlikTenant $destTenant -apiKey $destApiKey -tenantDomain $destTenantDomain
    
    #Create-DataConnection -SQLConn $SQLConn -managedSpaceNames $ManagedSpaceName -qlikTenant $destTenant -apiKey $destApiKey -tenantDomain $destTenantDomain
    
    Reload-Task -managedSpaceNames $ManagedSpaceName -qlikApps $QlikApps -tenantname $tenantUrl -apiKey $destApiKey 
    
    #$status = (Reload-Data -managedSpaceNames $ManagedSpaceName)
    #Write-Log -level INFO -message $status
    
    Write-Log -level INFO -message "Deployment completed for: $dbname `n"
    
    #Start-Sleep -Seconds 10
    
}
